# Trainers Guide

If it's your first Tor training as a trainer, this guide will provide some resources to help you.

## Follow Tor Project Code of Conduct and Best Practices

We want to create and promote a healthy, open and inclusive environment and community. 
So, first of all, make sure to read and follow our [Code of Conduct](https://community.torproject.org/training/code-of-conduct/). 

You will find how to prepare your training session in our [Best practices](https://community.torproject.org/training/best-practices/) document.

And remember:

 * Be as inclusive as possible
 * Keep yourself updated: make sure you have tried the latest Tor Browser, for example.
 * Be aware about the Internet laws in your country, as well as [verify if Tor Project website is blocked](https://explorer.ooni.org/) in your internet provider.

### Coordinate with the user researcher and Tor community team

We don't expect you to do all this work alone - we will support you with online meetings, guides and mentorship. 

You can join us any time on our [IRC channel](https://support.torproject.org/get-in-touch/#irc-help) #tor-south and our [mailing list](https://lists.torproject.org/cgi-bin/mailman/listinfo/global-south).

It will be great if you have someone to run the user researcher tests in your training. If you don't, let us know and we might find someone to do it in our community.

You and the user researcher should organize together the training agenda and also spare some time to interview the users, for example, reserve 1 hour at the end of the training. 

We will need you two, trainer and user researcher to coordinate to collect users feedbacks together.

## Logistics for planning a training

Verify with your community if they have an accessible space for the training.

If they don't, look for community centers or public spaces. But be aware that you're running a training for Human Rights Defenders, so make sure you will have a space that makes possible to keep the privacy of your training.

### Agenda

Organize an agenda for the training. It will be very helpful for you to follow the topics you want to share, and make sure that you're going over all the topics that you've planned.

Please share with us your agenda for the training, so we can provide feedback and tips.

### Communicate to engage!

Tell participants in advance what they should bring to the training. If they want to install Tor Browser for Desktop, it's important to bring their computer or an USB stick to copy Tor Browser. If they're going to install Tor Browser for Android, make sure they have ** at least 100 MB free in their memory device**, otherwise they won't be able to install it (and it can be very frustrating).

### Get prepared!

Print and bring the [Checklist for Trainers](FIXME) with you! You can also have it only in your computer, if you prefer.

Keep in mind that most people have never used Tor before, nor they know how it works. It can be complex, so let's keep it simple. You can download our slides for training from [Training resources](https://community.torproject.org/training/resources/).

Try to **keep it simple, and ensure to teach the basics**. It's better to understand how the Internet works, than to start with zero days and exploits.

Be a good listener. Your audience maybe shy to ask, so make sure you're giving them space and time to process all this new information, and ask their questions. 

If necessary, ask them from time to time if they have questions (we have some suggested moments to do so on our agenda).

When running your training, make sure you're following the steps listed in your checklist.

### Feedback

Before you wrap up the training, hand out post-its to your participants. 

You can give one post-it on each color and ask them to fill it with what they think about: 

1. The service they just learned
2. Questions about Tor project
3. Tor in general
4. General questions about privacy and security

Keep in mind that every feedback is a good feedback. It's very important for us to hear back from you.

We know that when it comes about training, practice makes it better. We want to know how the training was for you, how we can improve our support to engange and build your community and also, if you want to keep running Tor trainings. Please, [fill out this form](https://), byt the end of it, we will also ask your address to send to you a trainer kit (t-shirts and stickers).

We hope to hear back from you very soon! Have a great training!